__author__ = "Edward J. Stembler <mailto:ejstembler@mac.com>"
__copyright__ = "Copyright (c) 2009, Edward J. Stembler"
__license__ = "MIT"
__date__ = "$Apr 28, 2009 2:34 PM$"
__module_name__ = "Tests for QueryableList"
__version__ = "1.0"

from queryable_list import QueryableList
import unittest


class QueryableListTest(unittest.TestCase):

    def setUp(self):
        self.queryable_list = QueryableList(range(20))

    def tearDown(self):
        self.queryable_list = None

    def test_take_where_order_by_descending(self):
        expected_result = [4, 3, 2, 1, 0]
        result = self.queryable_list.take(10).where(lambda x: x < 5).order_by_descending()

        self.assertEqual(result, expected_result)

    def test_count(self):
        expected_result = 20
        result = self.queryable_list.count()

        self.assertEqual(result, expected_result)

    def test_count_func(self):
        expected_result = 2
        result = self.queryable_list.count(lambda x: x < 2)

        self.assertEqual(result, expected_result)

    def test_contains(self):
        self.assertEquals(self.queryable_list.contains(4), True)
        self.assertEquals(self.queryable_list.contains(21), False)

    def test_single(self):
        self.assertEqual(self.queryable_list.single(), 0)
        self.assertEqual(self.queryable_list.order_by_descending().single(), 19)

    def test_single_func(self):
        self.assertEqual(self.queryable_list.single(lambda x: x < 3), 0)
        self.assertEqual(self.queryable_list.single(lambda x: x > 10), 11)

        greater_than_100 = lambda x: x > 100
        self.assertRaises(ValueError, self.queryable_list.single, greater_than_100)

    def test_single_or_default(self):
        self.assertEqual(self.queryable_list.single_or_default(lambda x: x == 7), 7)
        self.assertEqual(self.queryable_list.single_or_default(lambda x: x == 30), None)

    def test_sum(self):
        self.assertEqual(self.queryable_list.sum(), 190)

    def test_sum_func(self):
        self.assertEqual(self.queryable_list.sum(lambda x: x > 10), 135)

    def test_average(self):
        self.assertEqual(self.queryable_list.average(), 9)


if __name__ == '__main__':
    unittest.main()
