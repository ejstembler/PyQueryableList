__author__ = "Edward J. Stembler <mailto:ejstembler@mac.com>"
__copyright__ = "Copyright (c) 2009, Edward J. Stembler"
__license__ = "MIT"
__date__ = "$Apr 6, 2009 11:48 PM$"
__module_name__ = "Queryable List"
__version__ = "1.0"

from itertools import ifilter
from itertools import imap
from itertools import islice
import UserList


def returns_new(func):
    def inner(self, args):
        return QueryableList(list(func(self, args)))

    return inner


class QueryableList(UserList.UserList):

    def __init__(self, data):
        super(QueryableList, self).__init__()
        self.data = data

    @returns_new
    def take(self, count):
        return islice(self.data, count)

    @returns_new
    def skip(self, count):
        return islice(self.data, count, None)

    @returns_new
    def join(self, stream):
        return ((left, right) for left in self.data for right in stream)

    @returns_new
    def select(self, func):
        return imap(func, self.data)

    @returns_new
    def where(self, func):
        return ifilter(func, self.data)

    def single_or_default(self, func=None):
        try:
            return self.data[0] if func is None else self.where(func).data[0]
        except IndexError:
            return None

    def single(self, func=None):
        result = self.single_or_default(func)
        if result is None or result == []:
            raise ValueError
        return result

    def order_by(self, func):
        self.data.sort(func)
        return QueryableList(self.data)

    def order_by_descending(self):
        return self.order_by(lambda x, y: y - x)

    def contains(self, item):
        return self.where(lambda x: x == item) != []

    def count(self, func=None):
        return len(self.data) if func is None else self.where(func).count()

    def sum(self, func=None):
        return sum(self.data) if func is None else self.where(func).sum()

    def average(self, func=None):
        return self.sum() / len(self.data) if func is None else self.where(func).average()
